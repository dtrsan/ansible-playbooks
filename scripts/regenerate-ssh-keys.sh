#!/bin/bash

DIR="$( cd -P "$( dirname "$0" )" && pwd )"

ROLES=( flink hadoop )

for ROLE in ${ROLES[@]}
do
  echo "Regenerating ssh keys for role $ROLE"
  ssh-keygen -q -f `realpath $DIR/../playbooks/roles/$ROLE/files/ssh-key-pair/id_rsa`
done

