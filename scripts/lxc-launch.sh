#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Launch lxc containers"
    echo "$0: usage: lxc-launch.sh <name> <n>"
    echo "    name - the base name of an instance"
    echo "    n - the number of instances to run"
    exit 1
fi

NAME=$1
N=$2

# Prepare init scripts/config
DIR="$( cd -P "$( dirname "$0" )" && pwd )"
INIT_SCRIPTS_DIR=$(mktemp -d -p /tmp flink-host-init.XXXXXX)
cp $DIR/host-init.sh ~/.ssh/id_rsa.pub $INIT_SCRIPTS_DIR
chmod 755 $INIT_SCRIPTS_DIR
chmod 444 $INIT_SCRIPTS_DIR/host-init.sh $INIT_SCRIPTS_DIR/id_rsa.pub


for i in `seq 1 $N`
do
  CNT_NAME=$NAME-$i
  lxc launch ubuntu:17.04 $CNT_NAME
done

# give some time so containers can start up fully
sleep 10

for i in `seq 1 $N`
do
  CNT_NAME=$NAME-$i
  # Setup ansible
  SRC=$INIT_SCRIPTS_DIR
  DEST=/tmp/host-init
  DISK_NAME=host-init
  lxc config device add $CNT_NAME $DISK_NAME disk source=$SRC path=$DEST
  lxc exec $CNT_NAME bash $DEST/host-init.sh ansible $DEST/id_rsa.pub
  lxc config device remove $CNT_NAME $DISK_NAME
done

# Cleanup
rm -rf $INIT_SCRIPTS_DIR

exit 0

