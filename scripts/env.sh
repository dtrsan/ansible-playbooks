DIR="$( cd -P "$( dirname "$0" )" && pwd )"

source $DIR/../ansible/bin/activate

export PATH=`realpath $DIR`:$PATH
