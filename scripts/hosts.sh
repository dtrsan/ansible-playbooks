#!/bin/bash

ROLES=( flink hadoop kafka )

for ROLE in ${ROLES[@]}
do
  echo "[$ROLE]"
  lxc list | \grep "$ROLE" | \grep RUNNING |cut -d'|' -f4 | cut -d' ' -f2
  echo ""
done

