#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Delete lxc containers"
    echo "$0: usage: lxc-launch.sh <name> <n>"
    echo "    name - the base name of an instance"
    echo "    n - the number of instances to run"
    exit 1
fi

NAME=$1
N=$2

for i in `seq 1 $N`
do
  CNT_NAME=$NAME-$i
  echo "Deleting $CNT_NAME"
  lxc stop $CNT_NAME
  lxc delete $CNT_NAME
done

