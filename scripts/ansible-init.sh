#!/bin/bash

DIR="$( cd -P "$( dirname "$0" )" && pwd )"
cd $DIR/..

virtualenv ansible
source ansible/bin/activate
pip install ansible


