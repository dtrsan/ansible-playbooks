# Miscellaneous ansible playbooks

## Scripts

### ansible-init.sh

Installs ansible to `ansible` directory inside ansible-playbooks root
directory. Run `source ansible/bin/activate` from the ansible-playbooks root
directory to initalize ansible enviroment.

### env.sh

Initialize ansible-playbooks enviroment.

### host-init.sh

Needs to be run on ansible host. Invoked by `lxc-launch.sh`.

### hosts.sh

Prints in ansible hosts format LXC containers IPv4 address for running
containers for all roles.

### lxc-delete.sh

Deletes LXC containers

### lxc-launch.sh

Creates and initializes LXC containers.

### regenerate-ssh-keys.sh

Regenerates all needed SSH keys used in playbooks


## Examples

Create 5 flink nodes and install Apache Flink on them.

```shell
source ./scripts/env.sh
lxc-launch.sh flink 5
regenerate-ssh-keys.sh
hosts.sh > playbooks/hosts-dev
cd playbooks
ansible-playbook -i hosts-dev -t flink -l flink site.yml
```

